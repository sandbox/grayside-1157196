<?php

/**
 * @file
 *  API for Roleset module.
 */

/**
 * Change defined rolesets per page load.
 *
 * Roleset plugin can be altered, such as modifying which roles a given roleset
 * contains.
 *
 * @param $rolesets
 *  Array of all known roleset objects.
 */
function hook_roleset_load_alter(&$rolesets) {
  // Once we resolve how to check any kind of context, this example will be elaborated.
  $rolesets['blog_writer']['roles'][] = roleset_role_machine_name(9);
}

/**
 * Determine which roleset realms should be loaded for the user account.
 *
 * This allows other modules to add additional roleset contexts for
 * the initial roleset load from the database.
 *
 * @param $account
 *  The user account object being checked.
 *
 * @return
 *  Array of realms to be checked for user rolesets.
 *  @code array('og' => array(12)) @endcode
 */
function hook_roleset_active_realms($account) {
  $group = og_get_group_context();

  return array(
    'og' => array(
      $group->nid,
    ),
  );
}

/**
 * Add or remove rolesets to the specified user per page load.
 *
 * This allows modules to arbitrarily add rolesets to user accounts
 * in a light-weight, fully dynamic way.
 *
 * @param $rolesets
 *  Array of all rolesets associated with the user account.
 * @param $account
 *  User account objects associated with the rolesets.
 */
function hook_roleset_user_rolesets_alter(&$rolesets, $account) {
  // 6 months in seconds.
  $timespan = 15778463;

  if (time() - $account->created >= $timespan) {
    $rolesets[] = 'journeyman_member';
  }
}


