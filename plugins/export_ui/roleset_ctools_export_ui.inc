<?php

/**
 * @file
 *  Create Export UI integration for Roleset module.
 */

$common_menu = array(
  'load arguments' => array('roleset_ctools_export_ui'),
  'access callback' => 'user_access',
  'access arguments' => array('administer rolesets'),
  // Set the file path to this include file.
  'file' => 'roleset_ctools_export_ui.inc',
  'file path' => drupal_get_path('module', 'roleset') . '/plugins/export_ui',
  'type' => MENU_LOCAL_TASK,
);

$plugin = array(
  'schema' => 'roleset',
  'user wizard' => 1,
  'menu' => array(
    'menu item' => 'roleset',
    'items' => array(
      // Add our own custom "Settings" page.
      'settings' => array(
        'path' => 'settings',
        'title' => 'Settings',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('roleset_ctools_export_ui_settings'),
      ) + $common_menu,
    ),
  ),

  'title' => t('Roleset'),
  'title singular' => t('roleset'),
  'title plural' => t('rolesets'),
  'title singular proper' => t('Roleset'),
  'title plural proper' => t('Rolesets'),
);

function roleset_ctools_export_ui_form(&$form, &$form_state) {
  $export = $form_state['item'];
  $form += array('info' => array('description' => ''), 'roleset' => '');

  $form['info']['title'] = array(
    '#title' => t('Title'),
    '#description' => t('The human-readable title of this roleset.'),
    '#type' => 'textfield',
    '#default_value' => !empty($export->title) ? $export->title : '',
  );

  $form['info']['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#default_value' => !empty($export->description) ? $export->description : '',
    '#description' => t('The roleset description.'),
  );

  // Not required because roles could be attached per page load.
  $options = drupal_map_assoc(roleset_rolename_get());
  $form['roles'] = array(
    '#title' => t('Roles'),
    '#type' => 'checkboxes',
    '#default_value' => !empty($export->roles) ? $export->roles : array(),
    '#options' => $options,
    '#description' => t('The roles associated with this roleset.'),
  );
}

/**
 * Settings page.
 */
function roleset_ctools_export_ui_settings($form_state) {
  $form = array();
  return system_settings_form($form);
}
