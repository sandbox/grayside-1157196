# Roleset

Allows roles to be grouped and assigned to users in batches per page load.

This module generalizes the basic concept of Organic Groups User Roles, and at
the same time provides a mechanism creating a better user-facing presentation
of "roles" to end-users and site managers.

   **Rolesets**         **Description**  
[] Blog Contributor     Create and edit your own blog posts.
[] Blog Moderator       Edit any blog post and control it's publishing status.

Add User [Grays_____________]   [Save]
          |        Grayside |
          |       Graysnide |
          |       Graysnips |
          -------------------   

## Maintainers
* Grayside <http://drupal.org/user/346868>
